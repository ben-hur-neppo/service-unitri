package br.edu.unitri.service.repository

import br.edu.unitri.service.domain.Student
import org.springframework.data.repository.CrudRepository

interface StudentRepository : CrudRepository<Student, String>
