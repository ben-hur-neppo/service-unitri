package br.edu.unitri.service.controller

import br.edu.unitri.service.domain.Student
import br.edu.unitri.service.service.StudentService
import mu.KLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("students")
class StudentController @Autowired constructor(val service: StudentService) {

    companion object : KLogging()

    @GetMapping("/count")
    fun countStudents(): Long {

        return service.countStudents()
    }

    @PostMapping
    fun saveStudent(@RequestBody student: Student) {

        logger.info { "Save Student[$student]]" }

        service.saveStudent(student)
    }

    @GetMapping
    fun getStudents(): List<Student> {

        return service.getStudent()
    }
}