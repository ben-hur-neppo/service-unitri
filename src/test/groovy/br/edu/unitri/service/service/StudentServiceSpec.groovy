package br.edu.unitri.service.service

import br.edu.unitri.service.domain.Student
import br.edu.unitri.service.repository.StudentRepository
import io.micrometer.core.instrument.MeterRegistry
import spock.lang.Specification

class StudentServiceSpec extends Specification {

    def "test find student"() {
        setup:
        def repository = Mock(StudentRepository)
        def registry = Mock(MeterRegistry)

        def service = new StudentService(repository, registry)

        def result

        when: "search for an existing student"
        result = service.getStudentByCode("john")

        then: "should work"
        1 * repository.findById("john") >> Optional.of(new Student(studentCode: "1", name: "john"))
        result.isPresent()
        result.get().name == "john"
        result.get().studentCode == "1"

        when: "search for a non existing student"
        result = service.getStudentByCode("mary")

        then: "should not work"
        1 * repository.findById("mary") >> Optional.empty()
        !result.isPresent()
    }
}