FROM airdock/oraclejdk:latest
VOLUME /tmp
COPY build/libs/*.jar /app.jar
ENTRYPOINT ["java"]
